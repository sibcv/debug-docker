# Debug Docker

Builds docker images for VA debugging

## Usage

```bash
./build.sh [OPTIONS]
Usage:
    -m, --mjpeg      Build with MJPEG libs
    -d, --drone      Build with drone libs
    -t, --tkrs       Build with TKRS libs

    -s, --save       Save as tar
    -z, --7z         Save as 7z (with tar inside)

    -h, --help       Show help screen
```
