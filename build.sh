#! /bin/bash


# PARSING ARGUMENTS

usage() {
    echo -e "Builds docker images for VA debugging"
    echo
    echo -e "Usage:"
    echo -e "\t-m, --mjpeg \t Build with MJPEG libs"
    echo -e "\t-d, --drone \t Build with drone libs"
    echo -e "\t-t, --tkrs  \t Build with TKRS libs"
    echo
    echo -e "\t-s, --save  \t Save as tar"
    echo -e "\t-z, --7z    \t Save as 7z (with tar inside)"
    echo
    echo -e "\t-h, --help  \t Show help screen"
}

while [ "$1" != "" ]; do
    case $1 in
        (-m | --mjpeg )         TOBUILD+=($MJPEG)
                                ;;
        (-d | --drone )         TOBUILD+=($DRONE)
                                ;;
        (-t | --tkrs )          TOBUILD+=($TKRS)
                                ;;
        (-s | --save )          SAVE=1
                                ;;
        (-z | --7z )            ZIP=1
                                ;;
        (-h | --help )          usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done


# DEFINING CONSTS

NAME=sibdebug

REQS=requirements
DCKS=dockerfiles

BASE=base
DRONE=drone
TKRS=tkrs
MJPEG=mjpeg

TOBUILD=()

SAVE=0
ZIP=0

VERSION=$BASE
for ver in $TOBUILD; do
    VERSION+=-$ver
done
TOBUILD_W_BASE=$TOBUILD
TOBUILD_W_BASE+=($BASE)

TAG=$NAME:$VERSION

TMP=tmp
SYSREQS=$TMP/sys.reqs
PIPREQS=$TMP/requirements.txt


# PREPARATION

mkdir -p $TMP
touch $SYSREQS
touch $PIPREQS


# MERGING REQUIREMENTS

for req in $TOBUILD_W_BASE; do
    for f in $REQS/$req/*.reqs; do
        (cat "${f}"; echo) >> $SYSREQS;
    done
    for f in $REQS/$req/python/requirements.txt; do
        (cat "${f}"; echo) >> $PIPREQS;
    done
done

awk '!a[$0]++' $SYSREQS > $SYSREQS.1
cat $SYSREQS.1 > $SYSREQS
rm -f $SYSREQS.1
awk '!a[$0]++' $PIPREQS > $PIPREQS.1
cat $PIPREQS.1 > $PIPREQS
rm -f $PIPREQS.1


# BUILDING IMAGE

echo "Building docker image" $TAG

docker build \
    -t $TAG \
    --build-arg SYSREQS=$SYSREQS\
    --build-arg PIPREQS=$PIPREQS\
    -f $DCKS/Dockerfile . \
    && \
    echo "Built docker image" $TAG "succesfully!"


# SAVING TO ARCHIVES

FILENAME=$NAME
for ver in $TOBUILD; do
    FILENAME+=_$ver
done

TAR_SAVE_LOC=./$FILENAME.tar
ZIP_SAVE_LOC=./$FILENAME.7z


# SAVING TO TAR

if [ "$SAVE" = "1" ]; then
    echo "Saving to tar"
    docker save $TAG > $TAR_SAVE_LOC
fi


# SAVING TO 7Z

if [ "$ZIP" = "1" ]; then
    if [ "$SAVE" = "0" ]; then
        echo "Saving to tar first"
        TAR_SAVE_LOC=$TMP/$TAR_SAVE_LOC
        docker save $TAG > TAR_SAVE_LOC
    fi
    echo "Saving to 7z"
    7z a $ZIP_SAVE_LOC $TAR_SAVE_LOC
fi


# CLEANUP

rm -rf tmp